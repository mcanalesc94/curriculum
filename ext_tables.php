<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Usuario.Usuario',
            'Usuario',
            'Usuario'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('usuario', 'Configuration/TypoScript', 'Usuario');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_usuario_domain_model_usuario', 'EXT:usuario/Resources/Private/Language/locallang_csh_tx_usuario_domain_model_usuario.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_usuario_domain_model_usuario');

    }
);
