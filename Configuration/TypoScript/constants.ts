
plugin.tx_usuario_usuario {
    view {
        # cat=plugin.tx_usuario_usuario/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:usuario/Resources/Private/Templates/
        # cat=plugin.tx_usuario_usuario/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:usuario/Resources/Private/Partials/
        # cat=plugin.tx_usuario_usuario/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:usuario/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_usuario_usuario//a; type=string; label=Default storage PID
        storagePid =
    }
}
