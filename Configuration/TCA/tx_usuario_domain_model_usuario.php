<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario',
        'label' => 'nombre',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'nombre,ecivil,fechanacim,rut,nacionalidad,comuna,titulo,rol,experiencia,renta_pretencion,renta_minima,celular,cv_path,email,mensaje,gerente,jefe_proyecto,consultor,arq_software,desarrollador,analista_funcional,analista_procesos,analista_calidad,tester_aplicaciones,disenador_grafico,ingeniero_sistemas,soporte_ti,otro',
        'iconfile' => 'EXT:usuario/Resources/Public/Icons/tx_usuario_domain_model_usuario.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, nombre, ecivil, fechanacim, rut, nacionalidad, comuna, titulo, rol, experiencia, renta_pretencion, renta_minima, celular, cv_path, email, mensaje, gerente, jefe_proyecto, consultor, arq_software, desarrollador, analista_funcional, analista_procesos, analista_calidad, tester_aplicaciones, disenador_grafico, ingeniero_sistemas, soporte_ti, otro',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, nombre, ecivil, fechanacim, rut, nacionalidad, comuna, titulo, rol, experiencia, renta_pretencion, renta_minima, celular, cv_path, email, mensaje, gerente, jefe_proyecto, consultor, arq_software, desarrollador, analista_funcional, analista_procesos, analista_calidad, tester_aplicaciones, disenador_grafico, ingeniero_sistemas, soporte_ti, otro, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_usuario_domain_model_usuario',
                'foreign_table_where' => 'AND tx_usuario_domain_model_usuario.pid=###CURRENT_PID### AND tx_usuario_domain_model_usuario.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'nombre' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.nombre',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'ecivil' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.ecivil',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'fechanacim' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.fechanacim',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 10,
                'eval' => 'datetime',
                'default' => time()
            ],
        ],
        'rut' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.rut',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'nacionalidad' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.nacionalidad',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'comuna' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.comuna',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'titulo' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.titulo',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'rol' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.rol',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'experiencia' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.experiencia',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'renta_pretencion' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.renta_pretencion',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'renta_minima' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.renta_minima',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'celular' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.celular',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'cv_path' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.cv_path',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'email' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'mensaje' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.mensaje',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]
        ],
        'gerente' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.gerente',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'jefe_proyecto' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.jefe_proyecto',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'consultor' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.consultor',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'arq_software' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.arq_software',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'desarrollador' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.desarrollador',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'analista_funcional' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.analista_funcional',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'analista_procesos' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.analista_procesos',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'analista_calidad' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.analista_calidad',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'tester_aplicaciones' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.tester_aplicaciones',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'disenador_grafico' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.disenador_grafico',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'ingeniero_sistemas' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.ingeniero_sistemas',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'soporte_ti' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.soporte_ti',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'otro' => [
            'exclude' => true,
            'label' => 'LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_domain_model_usuario.otro',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
    
    ],
];
