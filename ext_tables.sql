#
# Table structure for table 'tx_usuario_domain_model_usuario'
#
CREATE TABLE tx_usuario_domain_model_usuario (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	nombre varchar(255) DEFAULT '' NOT NULL,
	ecivil varchar(255) DEFAULT '' NOT NULL,
	fechanacim int(11) DEFAULT '0' NOT NULL,
	rut varchar(255) DEFAULT '' NOT NULL,
	nacionalidad varchar(255) DEFAULT '' NOT NULL,
	comuna varchar(255) DEFAULT '' NOT NULL,
	titulo varchar(255) DEFAULT '' NOT NULL,
	rol varchar(255) DEFAULT '' NOT NULL,
	experiencia int(11) DEFAULT '0' NOT NULL,
	renta_pretencion int(11) DEFAULT '0' NOT NULL,
	renta_minima int(11) DEFAULT '0' NOT NULL,
	celular varchar(255) DEFAULT '' NOT NULL,
	cv_path varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	mensaje text,
	gerente varchar(255) DEFAULT '' NOT NULL,
	jefe_proyecto varchar(255) DEFAULT '' NOT NULL,
	consultor varchar(255) DEFAULT '' NOT NULL,
	arq_software varchar(255) DEFAULT '' NOT NULL,
	desarrollador varchar(255) DEFAULT '' NOT NULL,
	analista_funcional varchar(255) DEFAULT '' NOT NULL,
	analista_procesos varchar(255) DEFAULT '' NOT NULL,
	analista_calidad varchar(255) DEFAULT '' NOT NULL,
	tester_aplicaciones varchar(255) DEFAULT '' NOT NULL,
	disenador_grafico varchar(255) DEFAULT '' NOT NULL,
	ingeniero_sistemas varchar(255) DEFAULT '' NOT NULL,
	soporte_ti varchar(255) DEFAULT '' NOT NULL,
	otro varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted smallint(5) unsigned DEFAULT '0' NOT NULL,
	hidden smallint(5) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state smallint(6) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state text,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);
