<?php
namespace Usuario\Usuario\Domain\Repository;

/***
 *
 * This file is part of the "usuario" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for Usuarios
 */
class UsuarioRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    }
