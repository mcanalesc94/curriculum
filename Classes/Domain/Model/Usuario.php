<?php
namespace Usuario\Usuario\Domain\Model;

/***
 *
 * This file is part of the "usuario" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Usuario
 */
class Usuario extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * nombre
     *
     * @var string
     */
    protected $nombre = '';

    /**
     * ecivil
     *
     * @var string
     */
    protected $ecivil = '';

    /**
     * fechanacim
     *
     * @var \DateTime
     */
    protected $fechanacim = '';

    /**
     * rut
     *
     * @var string
     */
    protected $rut = '';

    /**
     * nacionalidad
     *
     * @var string
     */
    protected $nacionalidad = '';

    /**
     * comuna
     *
     * @var string
     */
    protected $comuna = '';

    /**
     * titulo
     *
     * @var string
     */
    protected $titulo = '';

    /**
     * rol
     *
     * @var string
     */
    protected $rol = '';

    /**
     * experiencia
     *
     * @var int
     */
    protected $experiencia = '';

    /**
     * rentaPretencion
     *
     * @var int
     */
    protected $rentaPretencion = 0;

    /**
     * rentaMinima
     *
     * @var int
     */
    protected $rentaMinima = 0;

    /**
     * celular
     *
     * @var string
     */
    protected $celular = '';

    /**
     * cvPath
     *
     * @var string
     */
    protected $cvPath = '';

    /**
     * email
     *
     * @var string
     */
    protected $email = '';

    /**
     * mensaje
     *
     * @var string
     */
    protected $mensaje = '';

    /**
     * gerente
     *
     * @var string
     */
    protected $gerente = '';

    /**
     * jefeProyecto
     *
     * @var string
     */
    protected $jefeProyecto = '';

    /**
     * consultor
     *
     * @var string
     */
    protected $consultor = '';

    /**
     * arqSoftware
     *
     * @var string
     */
    protected $arqSoftware = '';

    /**
     * desarrollador
     *
     * @var string
     */
    protected $desarrollador = '';

    /**
     * analistaFuncional
     *
     * @var string
     */
    protected $analistaFuncional = '';

    /**
     * analistaProcesos
     *
     * @var string
     */
    protected $analistaProcesos = '';

    /**
     * analistaCalidad
     *
     * @var string
     */
    protected $analistaCalidad = '';

    /**
     * testerAplicaciones
     *
     * @var string
     */
    protected $testerAplicaciones = '';

    /**
     * disenadorGrafico
     *
     * @var string
     */
    protected $disenadorGrafico = '';

    /**
     * ingenieroSistemas
     *
     * @var string
     */
    protected $ingenieroSistemas = '';

    /**
     * soporteTi
     *
     * @var string
     */
    protected $soporteTi = '';

    /**
     * otro
     *
     * @var string
     */
    protected $otro = '';

    /**
     * Returns the nombre
     *
     * @return string $nombre
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Sets the nombre
     *
     * @param string $nombre
     * @return void
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Returns the ecivil
     *
     * @return string $ecivil
     */
    public function getEcivil()
    {
        return $this->ecivil;
    }

    /**
     * Sets the ecivil
     *
     * @param string $ecivil
     * @return void
     */
    public function setEcivil($ecivil)
    {
        $this->ecivil = $ecivil;
    }

    /**
     * Returns the nacionalidad
     *
     * @return string $nacionalidad
     */
    public function getNacionalidad()
    {
        return $this->nacionalidad;
    }

    /**
     * Sets the nacionalidad
     *
     * @param string $nacionalidad
     * @return void
     */
    public function setNacionalidad($nacionalidad)
    {
        $this->nacionalidad = $nacionalidad;
    }

    /**
     * Returns the rut
     *
     * @return string $rut
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * Sets the rut
     *
     * @param string $rut
     * @return void
     */
    public function setRut($rut)
    {
        $this->rut = $rut;
    }

    /**
     * Returns the comuna
     *
     * @return string $comuna
     */
    public function getComuna()
    {
        return $this->comuna;
    }

    /**
     * Sets the comuna
     *
     * @param string $comuna
     * @return void
     */
    public function setComuna($comuna)
    {
        $this->comuna = $comuna;
    }

    /**
     * Returns the titulo
     *
     * @return string $titulo
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Sets the titulo
     *
     * @param string $titulo
     * @return void
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * Returns the rol
     *
     * @return string $rol
     */
    public function getRol()
    {
        return $this->rol;
    }

    /**
     * Sets the rol
     *
     * @param string $rol
     * @return void
     */
    public function setRol($rol)
    {
        $this->rol = $rol;
    }

    /**
     * Returns the rentaPretencion
     *
     * @return int $rentaPretencion
     */
    public function getRentaPretencion()
    {
        return $this->rentaPretencion;
    }

    /**
     * Sets the rentaPretencion
     *
     * @param int $rentaPretencion
     * @return void
     */
    public function setRentaPretencion($rentaPretencion)
    {
        $this->rentaPretencion = $rentaPretencion;
    }

    /**
     * Returns the rentaMinima
     *
     * @return int $rentaMinima
     */
    public function getRentaMinima()
    {
        return $this->rentaMinima;
    }

    /**
     * Sets the rentaMinima
     *
     * @param int $rentaMinima
     * @return void
     */
    public function setRentaMinima($rentaMinima)
    {
        $this->rentaMinima = $rentaMinima;
    }

    /**
     * Returns the celular
     *
     * @return string $celular
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Sets the celular
     *
     * @param string $celular
     * @return void
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;
    }

    /**
     * Returns the cvPath
     *
     * @return string $cvPath
     */
    public function getCvPath()
    {
        return $this->cvPath;
    }

    /**
     * Sets the cvPath
     *
     * @param string $cvPath
     * @return void
     */
    public function setCvPath($cvPath)
    {
        $this->cvPath = $cvPath;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the mensaje
     *
     * @return string $mensaje
     */
    public function getMensaje()
    {
        return $this->mensaje;
    }

    /**
     * Sets the mensaje
     *
     * @param string $mensaje
     * @return void
     */
    public function setMensaje($mensaje)
    {
        $this->mensaje = $mensaje;
    }

    /**
     * Returns the experiencia
     *
     * @return int experiencia
     */
    public function getExperiencia()
    {
        return $this->experiencia;
    }

    /**
     * Sets the experiencia
     *
     * @param string $experiencia
     * @return void
     */
    public function setExperiencia($experiencia)
    {
        $this->experiencia = $experiencia;
    }

    /**
     * Returns the gerente
     *
     * @return string $gerente
     */
    public function getGerente()
    {
        return $this->gerente;
    }

    /**
     * Sets the gerente
     *
     * @param string $gerente
     * @return void
     */
    public function setGerente($gerente)
    {
        $this->gerente = $gerente;
    }

    /**
     * Returns the jefeProyecto
     *
     * @return string $jefeProyecto
     */
    public function getJefeProyecto()
    {
        return $this->jefeProyecto;
    }

    /**
     * Sets the jefeProyecto
     *
     * @param string $jefeProyecto
     * @return void
     */
    public function setJefeProyecto($jefeProyecto)
    {
        $this->jefeProyecto = $jefeProyecto;
    }

    /**
     * Returns the consultor
     *
     * @return string $consultor
     */
    public function getConsultor()
    {
        return $this->consultor;
    }

    /**
     * Sets the consultor
     *
     * @param string $consultor
     * @return void
     */
    public function setConsultor($consultor)
    {
        $this->consultor = $consultor;
    }

    /**
     * Returns the arqSoftware
     *
     * @return string $arqSoftware
     */
    public function getArqSoftware()
    {
        return $this->arqSoftware;
    }

    /**
     * Sets the arqSoftware
     *
     * @param string $arqSoftware
     * @return void
     */
    public function setArqSoftware($arqSoftware)
    {
        $this->arqSoftware = $arqSoftware;
    }

    /**
     * Returns the desarrollador
     *
     * @return string $desarrollador
     */
    public function getDesarrollador()
    {
        return $this->desarrollador;
    }

    /**
     * Sets the desarrollador
     *
     * @param string $desarrollador
     * @return void
     */
    public function setDesarrollador($desarrollador)
    {
        $this->desarrollador = $desarrollador;
    }

    /**
     * Returns the analistaFuncional
     *
     * @return string $analistaFuncional
     */
    public function getAnalistaFuncional()
    {
        return $this->analistaFuncional;
    }

    /**
     * Sets the analistaFuncional
     *
     * @param string $analistaFuncional
     * @return void
     */
    public function setAnalistaFuncional($analistaFuncional)
    {
        $this->analistaFuncional = $analistaFuncional;
    }

    /**
     * Returns the analistaProcesos
     *
     * @return string $analistaProcesos
     */
    public function getAnalistaProcesos()
    {
        return $this->analistaProcesos;
    }

    /**
     * Sets the analistaProcesos
     *
     * @param string $analistaProcesos
     * @return void
     */
    public function setAnalistaProcesos($analistaProcesos)
    {
        $this->analistaProcesos = $analistaProcesos;
    }

    /**
     * Returns the analistaCalidad
     *
     * @return string $analistaCalidad
     */
    public function getAnalistaCalidad()
    {
        return $this->analistaCalidad;
    }

    /**
     * Sets the analistaCalidad
     *
     * @param string $analistaCalidad
     * @return void
     */
    public function setAnalistaCalidad($analistaCalidad)
    {
        $this->analistaCalidad = $analistaCalidad;
    }

    /**
     * Returns the testerAplicaciones
     *
     * @return string $testerAplicaciones
     */
    public function getTesterAplicaciones()
    {
        return $this->testerAplicaciones;
    }

    /**
     * Sets the testerAplicaciones
     *
     * @param string $testerAplicaciones
     * @return void
     */
    public function setTesterAplicaciones($testerAplicaciones)
    {
        $this->testerAplicaciones = $testerAplicaciones;
    }

    /**
     * Returns the disenadorGrafico
     *
     * @return string $disenadorGrafico
     */
    public function getDisenadorGrafico()
    {
        return $this->disenadorGrafico;
    }

    /**
     * Sets the disenadorGrafico
     *
     * @param string $disenadorGrafico
     * @return void
     */
    public function setDisenadorGrafico($disenadorGrafico)
    {
        $this->disenadorGrafico = $disenadorGrafico;
    }

    /**
     * Returns the ingenieroSistemas
     *
     * @return string $ingenieroSistemas
     */
    public function getIngenieroSistemas()
    {
        return $this->ingenieroSistemas;
    }

    /**
     * Sets the ingenieroSistemas
     *
     * @param string $ingenieroSistemas
     * @return void
     */
    public function setIngenieroSistemas($ingenieroSistemas)
    {
        $this->ingenieroSistemas = $ingenieroSistemas;
    }

    /**
     * Returns the soporteTi
     *
     * @return string $soporteTi
     */
    public function getSoporteTi()
    {
        return $this->soporteTi;
    }

    /**
     * Sets the soporteTi
     *
     * @param string $soporteTi
     * @return void
     */
    public function setSoporteTi($soporteTi)
    {
        $this->soporteTi = $soporteTi;
    }

    /**
     * Returns the otro
     *
     * @return string $otro
     */
    public function getOtro()
    {
        return $this->otro;
    }

    /**
     * Sets the otro
     *
     * @param string $otro
     * @return void
     */
    public function setOtro($otro)
    {
        $this->otro = $otro;
    }

    /**
     * Returns the fechanacim
     *
     * @return \DateTime fechanacim
     */
    public function getFechanacim()
    {
        return $this->fechanacim;
    }

    /**
     * Sets the fechanacim
     *
     * @param string $fechanacim
     * @return void
     */
    public function setFechanacim($fechanacim)
    {
        $this->fechanacim = $fechanacim;
    }
}
