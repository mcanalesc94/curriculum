<?php
namespace Usuario\Usuario\Controller;

/***
 *
 * This file is part of the "usuario" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * UsuarioController
 */
class UsuarioController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * usuarioRepository
     *
     * @var \Usuario\Usuario\Domain\Repository\UsuarioRepository
     * @inject
     */
    protected $usuarioRepository = null;

    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {

    }

    /**
     * action create
     *
     * @param \Usuario\Usuario\Domain\Model\Usuario $newUsuario
     * @return void
     */
    public function createAction(\Usuario\Usuario\Domain\Model\Usuario $newUsuario)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->usuarioRepository->add($newUsuario);
        $this->redirect('list');
    }

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $usuarios = $this->usuarioRepository->findAll();
        $this->view->assign('usuarios', $usuarios);
    }

    /**
     * action show
     *
     * @param \Usuario\Usuario\Domain\Model\Usuario $usuario
     * @return void
     */
    public function showAction(\Usuario\Usuario\Domain\Model\Usuario $usuario)
    {
        $this->view->assign('usuario', $usuario);
    }

    /**
     * action edit
     *
     * @param \Usuario\Usuario\Domain\Model\Usuario $usuario
     * @ignorevalidation $usuario
     * @return void
     */
    public function editAction(\Usuario\Usuario\Domain\Model\Usuario $usuario)
    {
        $this->view->assign('usuario', $usuario);
    }

    /**
     * action update
     *
     * @param \Usuario\Usuario\Domain\Model\Usuario $usuario
     * @return void
     */
    public function updateAction(\Usuario\Usuario\Domain\Model\Usuario $usuario)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->usuarioRepository->update($usuario);
        $this->redirect('list');
    }
}
