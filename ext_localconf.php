<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Usuario.Usuario',
            'Usuario',
            [
                'Usuario' => 'list, show, new, create, edit, update'
            ],
            // non-cacheable actions
            [
                'Usuario' => 'list, show, new, create, edit, update'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    usuario {
                        iconIdentifier = usuario-plugin-usuario
                        title = LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_usuario.name
                        description = LLL:EXT:usuario/Resources/Private/Language/locallang_db.xlf:tx_usuario_usuario.description
                        tt_content_defValues {
                            CType = list
                            list_type = usuario_usuario
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'usuario-plugin-usuario',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:usuario/Resources/Public/Icons/user_plugin_usuario.svg']
			);
		
    }
);
