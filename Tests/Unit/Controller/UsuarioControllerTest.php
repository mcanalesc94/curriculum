<?php
namespace Usuario\Usuario\Tests\Unit\Controller;

/**
 * Test case.
 */
class UsuarioControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Usuario\Usuario\Controller\UsuarioController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Usuario\Usuario\Controller\UsuarioController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllUsuariosFromRepositoryAndAssignsThemToView()
    {

        $allUsuarios = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $usuarioRepository = $this->getMockBuilder(\Usuario\Usuario\Domain\Repository\UsuarioRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $usuarioRepository->expects(self::once())->method('findAll')->will(self::returnValue($allUsuarios));
        $this->inject($this->subject, 'usuarioRepository', $usuarioRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('usuarios', $allUsuarios);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenUsuarioToView()
    {
        $usuario = new \Usuario\Usuario\Domain\Model\Usuario();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('usuario', $usuario);

        $this->subject->showAction($usuario);
    }

    /**
     * @test
     */
    public function createActionAddsTheGivenUsuarioToUsuarioRepository()
    {
        $usuario = new \Usuario\Usuario\Domain\Model\Usuario();

        $usuarioRepository = $this->getMockBuilder(\Usuario\Usuario\Domain\Repository\UsuarioRepository::class)
            ->setMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $usuarioRepository->expects(self::once())->method('add')->with($usuario);
        $this->inject($this->subject, 'usuarioRepository', $usuarioRepository);

        $this->subject->createAction($usuario);
    }

    /**
     * @test
     */
    public function editActionAssignsTheGivenUsuarioToView()
    {
        $usuario = new \Usuario\Usuario\Domain\Model\Usuario();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('usuario', $usuario);

        $this->subject->editAction($usuario);
    }

    /**
     * @test
     */
    public function updateActionUpdatesTheGivenUsuarioInUsuarioRepository()
    {
        $usuario = new \Usuario\Usuario\Domain\Model\Usuario();

        $usuarioRepository = $this->getMockBuilder(\Usuario\Usuario\Domain\Repository\UsuarioRepository::class)
            ->setMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $usuarioRepository->expects(self::once())->method('update')->with($usuario);
        $this->inject($this->subject, 'usuarioRepository', $usuarioRepository);

        $this->subject->updateAction($usuario);
    }
}
