<?php
namespace Usuario\Usuario\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class UsuarioTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Usuario\Usuario\Domain\Model\Usuario
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Usuario\Usuario\Domain\Model\Usuario();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getNombreReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getNombre()
        );
    }

    /**
     * @test
     */
    public function setNombreForStringSetsNombre()
    {
        $this->subject->setNombre('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'nombre',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEcivilReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getEcivil()
        );
    }

    /**
     * @test
     */
    public function setEcivilForStringSetsEcivil()
    {
        $this->subject->setEcivil('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'ecivil',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFechanacimReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getFechanacim()
        );
    }

    /**
     * @test
     */
    public function setFechanacimForDateTimeSetsFechanacim()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setFechanacim($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'fechanacim',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRutReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRut()
        );
    }

    /**
     * @test
     */
    public function setRutForStringSetsRut()
    {
        $this->subject->setRut('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'rut',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getNacionalidadReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getNacionalidad()
        );
    }

    /**
     * @test
     */
    public function setNacionalidadForStringSetsNacionalidad()
    {
        $this->subject->setNacionalidad('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'nacionalidad',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getComunaReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getComuna()
        );
    }

    /**
     * @test
     */
    public function setComunaForStringSetsComuna()
    {
        $this->subject->setComuna('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'comuna',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTituloReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitulo()
        );
    }

    /**
     * @test
     */
    public function setTituloForStringSetsTitulo()
    {
        $this->subject->setTitulo('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'titulo',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRolReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRol()
        );
    }

    /**
     * @test
     */
    public function setRolForStringSetsRol()
    {
        $this->subject->setRol('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'rol',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getExperienciaReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getExperiencia()
        );
    }

    /**
     * @test
     */
    public function setExperienciaForIntSetsExperiencia()
    {
        $this->subject->setExperiencia(12);

        self::assertAttributeEquals(
            12,
            'experiencia',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRentaPretencionReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getRentaPretencion()
        );
    }

    /**
     * @test
     */
    public function setRentaPretencionForIntSetsRentaPretencion()
    {
        $this->subject->setRentaPretencion(12);

        self::assertAttributeEquals(
            12,
            'rentaPretencion',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRentaMinimaReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getRentaMinima()
        );
    }

    /**
     * @test
     */
    public function setRentaMinimaForIntSetsRentaMinima()
    {
        $this->subject->setRentaMinima(12);

        self::assertAttributeEquals(
            12,
            'rentaMinima',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCelularReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getCelular()
        );
    }

    /**
     * @test
     */
    public function setCelularForStringSetsCelular()
    {
        $this->subject->setCelular('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'celular',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCvPathReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getCvPath()
        );
    }

    /**
     * @test
     */
    public function setCvPathForStringSetsCvPath()
    {
        $this->subject->setCvPath('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'cvPath',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEmailReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getEmail()
        );
    }

    /**
     * @test
     */
    public function setEmailForStringSetsEmail()
    {
        $this->subject->setEmail('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'email',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMensajeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getMensaje()
        );
    }

    /**
     * @test
     */
    public function setMensajeForStringSetsMensaje()
    {
        $this->subject->setMensaje('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'mensaje',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getGerenteReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getGerente()
        );
    }

    /**
     * @test
     */
    public function setGerenteForStringSetsGerente()
    {
        $this->subject->setGerente('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'gerente',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getJefeProyectoReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getJefeProyecto()
        );
    }

    /**
     * @test
     */
    public function setJefeProyectoForStringSetsJefeProyecto()
    {
        $this->subject->setJefeProyecto('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'jefeProyecto',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getConsultorReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getConsultor()
        );
    }

    /**
     * @test
     */
    public function setConsultorForStringSetsConsultor()
    {
        $this->subject->setConsultor('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'consultor',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getArqSoftwareReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getArqSoftware()
        );
    }

    /**
     * @test
     */
    public function setArqSoftwareForStringSetsArqSoftware()
    {
        $this->subject->setArqSoftware('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'arqSoftware',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDesarrolladorReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDesarrollador()
        );
    }

    /**
     * @test
     */
    public function setDesarrolladorForStringSetsDesarrollador()
    {
        $this->subject->setDesarrollador('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'desarrollador',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAnalistaFuncionalReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getAnalistaFuncional()
        );
    }

    /**
     * @test
     */
    public function setAnalistaFuncionalForStringSetsAnalistaFuncional()
    {
        $this->subject->setAnalistaFuncional('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'analistaFuncional',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAnalistaProcesosReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getAnalistaProcesos()
        );
    }

    /**
     * @test
     */
    public function setAnalistaProcesosForStringSetsAnalistaProcesos()
    {
        $this->subject->setAnalistaProcesos('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'analistaProcesos',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAnalistaCalidadReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getAnalistaCalidad()
        );
    }

    /**
     * @test
     */
    public function setAnalistaCalidadForStringSetsAnalistaCalidad()
    {
        $this->subject->setAnalistaCalidad('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'analistaCalidad',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTesterAplicacionesReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTesterAplicaciones()
        );
    }

    /**
     * @test
     */
    public function setTesterAplicacionesForStringSetsTesterAplicaciones()
    {
        $this->subject->setTesterAplicaciones('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'testerAplicaciones',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDisenadorGraficoReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDisenadorGrafico()
        );
    }

    /**
     * @test
     */
    public function setDisenadorGraficoForStringSetsDisenadorGrafico()
    {
        $this->subject->setDisenadorGrafico('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'disenadorGrafico',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIngenieroSistemasReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getIngenieroSistemas()
        );
    }

    /**
     * @test
     */
    public function setIngenieroSistemasForStringSetsIngenieroSistemas()
    {
        $this->subject->setIngenieroSistemas('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'ingenieroSistemas',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSoporteTiReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getSoporteTi()
        );
    }

    /**
     * @test
     */
    public function setSoporteTiForStringSetsSoporteTi()
    {
        $this->subject->setSoporteTi('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'soporteTi',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getOtroReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getOtro()
        );
    }

    /**
     * @test
     */
    public function setOtroForStringSetsOtro()
    {
        $this->subject->setOtro('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'otro',
            $this->subject
        );
    }
}
